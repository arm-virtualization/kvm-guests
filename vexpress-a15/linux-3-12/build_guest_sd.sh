#!/bin/bash

#######
# Change according to your system configuration...
export ARCH=arm
export CROSS_COMPILE=arm-linux-gnueabihf-
BUILD_NB_THREADS=8
#######

# No need to modify these variables

KERNEL_BUILD_DIR=linux-3-12-src
KERNEL_GIT_SRC=git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git
KERNEL_GIT_BRANCH=origin/linux-3.12.y
KERNEL_DEFCONFIG=vexpress_defconfig
KERNEL_CONFIG_FILE=linux-config-vexpress-a15-sdcard
INST_PATH=linux-bins-sd
MODULES_INST_PATH=${INST_PATH}/modules
DTB_FILE=${KERNEL_BUILD_DIR}/arch/arm/boot/dts/vexpress-v2p-ca15-tc1.dtb


# include commeon kernel guest build file
source ../../build_guest_common_functions.sh


# execute main function
main
